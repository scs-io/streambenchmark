* Production Rate
** Configured with $LOAD environment variable
** As a throughput in tuples per second
* DONE Consumption Rate
** Slow down as a flinkBenchmarkParams parameter in */flink/benchmark/AdvertisingTopologyNative.java
** Internally, slow down or speed up via use of flink.buffer-timeout parameter
** Configured with $CRATE environment variable
** As a throughput in bits per second
* DONE Size of Message
** Messages are strings of length 237 without time digits
** Configured with $MSG_PAD environment variable
** As number of space characters appended to the end of the json message
* TODO Number of Producers
** mpi
* TODO Number of Consumers
** Play with Flink and see how do multiprocess stream processing
*** Messages are sent to one Kafka topic. Needs to be different. How?
* TODO Number of Topics
** Char separated string for Topics to be passed and broken in program
** RECOMMENDED: Array ideally 
* Q: What does the consumer do?
** A: This
#+BEGIN_SRC java
  DataStream<String> messageStream = env
      .addSource(new FlinkKafkaConsumer082<String>(
                                                   flinkBenchmarkParams.getRequired("topic"),
                                                   new SimpleStringSchema(),
                                                   flinkBenchmarkParams.getProperties())).setParallelism(Math.min(hosts * cores, kafkaPartitions));

  messageStream
      .rebalance()
  // Parse the String as JSON
      .flatMap(new DeserializeBolt())

  //Filter the records if event type is "view"
      .filter(new EventFilterBolt())

  // project the event
      .<Tuple2<String, String>>project(2, 5)

  // perform join with redis data
      .flatMap(new RedisJoinBolt())

  // process campaign
      .keyBy(0)
      .flatMap(new CampaignProcessor());
#+END_SRC
* Q: What kind of events are generated?
** A: 
